# Street Fighter

## --- Docker ---

Image - rmelamud/street-fighter

### Start

- `docker-compose up -d`
- open http://localhost:8080/

## --- Local ---

### Setup:

- `npm install`

### Start:

- `npm start`
- open http://localhost:8080/

### Build:

#### Webpack:

- `npm run build`

#### TypeScript:

- `npm run build-ts`

### Game controls:

#### First fighter:

- Attack: `A`
- Defend: `D`
- Criticat hit combination: `Q`, `W`, `E`

#### Second fighter:

- Attack: `J`
- Defend: `L`
- Critical hit combination: `U`, `I`, `O`
