const path = require("path");
const express = require("express");

const port = process.env.PORT ? parseInt(process.env.PORT) : 8080;

const indexPath = path.join(__dirname, "index.html");
const jsPath = path.join(__dirname, "dist");
const stylesPath = path.join(__dirname, "styles");
const logoPath = path.join(__dirname, "resources", "logo.png");

const app = express();

app.use("/dist", express.static(jsPath));
app.use("/styles", express.static(stylesPath));
app.get("/resources/logo.png", (req, res) => res.sendFile(logoPath));

app.get("/", (req, res) => res.sendFile(indexPath));
app.listen(port);
