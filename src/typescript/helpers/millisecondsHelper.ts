export default function millis(seconds: number): number {
	return seconds * 1000;
}
