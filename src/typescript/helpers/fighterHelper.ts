import { CRITICAL_HIT_DELAY_MS } from "../constants/controls";
import { FighterDetails } from "../typings/entities";
import GameFighter from "../typings/GameFighter";

export function fighterDetailsToGameFighter(
	fighter: FighterDetails
): GameFighter {
	return {
		...fighter,
		isInBlock: false,
		currentHealth: fighter.health,
		lastCriticalHitMs: performance.now() - CRITICAL_HIT_DELAY_MS,
	};
}
