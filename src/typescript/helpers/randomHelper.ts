import { RANDOM_ADD_DIVIDER } from "../constants/random";

export function random(inclusiveMin: number, exclusiveMax: number): number {
	const multiplier = exclusiveMax - inclusiveMin;
	const random = Math.random();
	const addRandom = Math.random() / RANDOM_ADD_DIVIDER;
	const result = inclusiveMin + random * multiplier + addRandom;
	const exclusiveMaxTreshold = Math.min(result, exclusiveMax);

	return exclusiveMaxTreshold;
}

export function chance(): number {
	return random(1, 2);
}
