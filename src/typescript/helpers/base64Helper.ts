export function decodeBase64(base64String: string): string {
	return Buffer.from(base64String, "base64").toString();
}

export function decodeBase64AsObject<T>(base64String: string): T {
	return JSON.parse(Buffer.from(base64String, "base64").toString()) as T;
}
