import { createFighter } from "./fighterView";
import { showFighterDetailsModal } from "./modals/fighterDetails";
import { createElement } from "./helpers/domHelper";
import { fight } from "./fight";
import { showWinnerModal } from "./modals/winner";
import { Fighter, FighterDetails } from "./typings/entities";
import { getFighterDetails } from "./services/fightersService";

const fighterElementsMap = new Map<string, HTMLElement>();

export function createFighters(fighters: Fighter[]): HTMLElement {
	const selectFighterForBattle = createFightersSelector();

	const fighterElements = fighters.map((fighter) => {
		const element = createFighter(
			fighter,
			showFighterDetails,
			selectFighterForBattle
		);

		fighterElementsMap.set(fighter._id, element);
		return element;
	});

	const fightersContainer = createElement({
		tagName: "div",
		className: "fighters",
	});

	fightersContainer.append(...fighterElements);
	return fightersContainer;
}

const fightersDetailsCache = new Map();

async function showFighterDetails(
	_event: Event,
	fighter: Fighter
): Promise<void> {
	const fullInfo: FighterDetails = await getFighterInfo(fighter._id);
	showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(
	fighterId: string
): Promise<FighterDetails> {
	if (fightersDetailsCache.has(fighterId)) {
		return fightersDetailsCache.get(fighterId);
	} else {
		const fighter = await getFighterDetails(fighterId);
		fightersDetailsCache.set(fighterId, fighter);

		return fighter;
	}
}

function createFightersSelector() {
	const selectedFighters = new Map<string, FighterDetails>();

	return async function selectFighterForBattle(event: Event, fighter: Fighter) {
		const fullInfo: FighterDetails = await getFighterInfo(fighter._id);
		const checkbox = event.target as { checked: boolean } | null;

		if (checkbox?.checked) {
			selectedFighters.set(fighter._id, fullInfo);
		} else {
			selectedFighters.delete(fighter._id);
		}

		if (selectedFighters.size === 2) {
			const fighters = [...selectedFighters.values()];
			const selectedFighterIds = fighters.map((f) => f._id);
			const fighterIds = [...fighterElementsMap.keys()];
			const healthElementsMap = new Map<string, HTMLElement>();

			fighterIds.forEach((id) => {
				if (selectedFighterIds.includes(id)) {
					const element = fighterElementsMap.get(id) as HTMLElement;
					element.removeChild(element.childNodes[2]);

					const healthElement = createElement({ tagName: "div" });
					const health = selectedFighters.get(id)?.health;

					healthElement.innerHTML = `Health: ${health}`;
					healthElementsMap.set(id, healthElement);
					element.append(healthElement);
				} else {
					fighterElementsMap.get(id)?.remove();
				}
			});

			fighterElementsMap
				.get(selectedFighterIds[0])
				?.classList.add("flex-first");

			fighterElementsMap
				.get(selectedFighterIds[1])
				?.classList.add("invert-image");

			const winner: FighterDetails = await fight(
				fighters[0],
				fighters[1],
				(value: number) => {
					const element = healthElementsMap.get(fighters[0]._id) as HTMLElement;
					element.innerHTML = `Health: ${value}`;
				},
				(value: number) => {
					const element = healthElementsMap.get(fighters[1]._id) as HTMLElement;
					element.innerHTML = `Health: ${value}`;
				}
			);

			showWinnerModal(winner);
		}
	};
}
