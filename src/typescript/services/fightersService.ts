import callApi from "../helpers/apiHelper";
import { Fighter, FighterDetails } from "../typings/entities";
import HttpMethod from "../typings/HttpMethod";

export async function getFighters(): Promise<Fighter[]> {
	const endpoint = "fighters.json";
	const apiResult = await callApi<Fighter[]>(endpoint, HttpMethod.Get);

	return apiResult;
}

export async function getFighterDetails(id: string): Promise<FighterDetails> {
	const endpoint = `details/fighter/${id}.json`;
	const apiResult = await callApi<FighterDetails>(endpoint, HttpMethod.Get);

	return apiResult;
}
