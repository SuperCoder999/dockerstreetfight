export default class HttpError extends Error {
	public constructor(message: string, status: number) {
		super(`${status}: ${message}`);
	}
}
