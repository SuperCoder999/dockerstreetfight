export default interface DomElement {
	tagName: string;
	className?: string;
	attributes?: Record<string, string>;
}
