FROM node:latest

COPY ./src ./src
COPY ./resources ./resources
COPY ./index.* ./
COPY ./package.json ./
COPY ./server.js ./
COPY ./tsconfig.json ./
COPY ./webpack.config.js ./

ENV PORT=8080

RUN npm install
RUN npm run build
CMD ["npm", "start"]

EXPOSE 8080
